#!/bin/env python

class hotspot(object):

	def __init__(self):

		self.avg_pos = -1
		self.weighted_avg_sd = 0.0
		self.dens_count = 0.0
		self.max_window_size = -1

		# used for windowing
		self.filter_left = -1
		self.filter_right = -1
		self.filter_tags = 0
		self.filter_width = 0
	
		#used for statistics
		self.filter_z_score = 0.0
		self.filter_p_value = 1.0

# fast function to find the left most element in an array (a) that
# is greater or equal to x

import bisect

def find_ge(a, x):

	i = bisect.bisect_left(a, x)

	if i != len(a):
    
		return i

	raise ValueError

import numpy as np

# routine finds individual hotspots that pass threshold
# from what I understand this function looks at different window sizes
# around each cleavage site and tests whether their is more cleavages
# than 3 standard deviations from the expected.

def find_hotspots(tags_count, contig_size, sd_cutoff = 3.0, window_min = 200, window_max = 300, window_step = 50, window_thresh_fuzzy = False):

	hotspots = {}

	# sort the dict keys

	uniq_positions = np.array( sorted(tags_count.keys()) )

	tags_count_total = np.sum( tags_count.values() )

	for window_size in xrange(window_min, window_max + 1, window_step):
				
		start = 0

		p = float(window_size) / float(contig_size) #proportion of genome size
		mean = p * float(tags_count_total) # expected tag counts
		sd = np.sqrt( p * (1.0-p) * float(tags_count_total) ) #expected sd

		thresh = 1.0 + mean + sd * sd_cutoff

		for pos in uniq_positions:

			total_within = 0
			pos_avg_within = 0

			# set points around the window

			left = pos - (window_size / 2)
			right =  pos + (window_size / 2)

			start += 1 if left > uniq_positions[start] and start < len(uniq_positions) else 0

			i = start

			while i < len(uniq_positions) and uniq_positions[i] <= right:

				if uniq_positions[i] >= left and uniq_positions[i] <= right:

					total_within += tags_count.get(uniq_positions[i], 0)
					pos_avg_within += uniq_positions[i] * tags_count.get(uniq_positions[i], 0)

				i += 1

			# adjust thresh if in "fuzzy" mode; not really sure why this is implemented
			# this allows some weak DHSs to squeak through

			adj_thresh = (thresh + np.random.random() - 0.5) if window_thresh_fuzzy and np.abs(total_within - thresh) <= 0.5 else thresh

			# call a putative hotspot

			if total_within > adj_thresh:

				obs_sd = (float(total_within) - 1.0 - mean) / sd

				if not pos in hotspots: hotspots[pos] = hotspot()

				hotspots[pos].dens_count += 1
				hotspots[pos].weighted_avg_sd += obs_sd
				hotspots[pos].avg_pos = pos_avg_within / total_within
				hotspots[pos].max_window_size = window_size

		# loop for genome sites
	
	# loop window sizes

	for pos, h in hotspots.items():

		h.weighted_avg_sd /= h.dens_count

	return hotspots

# filter the hotspots using density

def filter_hotspots(hotspots):

	filtered_hotspots = {}

	n  = 0

	sorted_hotspot_positions = sorted(hotspots.keys())

	# init with first

	current = hotspots[sorted_hotspot_positions[0]]

	filtered = hotspot()
	filtered.filter_width = current.max_window_size

	filtered_hotspots[n] = filtered

	prev_center = current.avg_pos
	adj_center = prev_center
	avg_dens_count = current.dens_count
	avg_sd = current.weighted_avg_sd
			
	cluster_size = 1

	# go through the rest in sequential order; merging when necesary

	for pos in sorted_hotspot_positions[1:]:

		current = hotspots[pos]

		# merge clusters if they overlap by the maximum window size

		if np.abs(prev_center - current.avg_pos) < current.max_window_size:

			filtered_hotspots[n].filter_right = pos
			filtered_hotspots[n].filter_width += current.max_window_size

			adj_center += current.avg_pos
			avg_dens_count += current.dens_count
			avg_sd += current.weighted_avg_sd

			cluster_size += 1

		# if not emit the cluster and start a new cluster

		else:

			filtered_hotspots[n].avg_pos = adj_center / cluster_size
			filtered_hotspots[n].filter_width /= cluster_size
			filtered_hotspots[n].dens_count = avg_dens_count / cluster_size
			filtered_hotspots[n].weighted_avg_sd = avg_sd / cluster_size

			n += 1

			filtered = hotspot()
			filtered.filter_width = current.max_window_size

			filtered_hotspots[n] = filtered

			prev_center = current.avg_pos
			adj_center = prev_center
			avg_dens_count = current.dens_count
			avg_sd = current.weighted_avg_sd
			
			cluster_size = 1

	
	filtered_hotspots[n].avg_pos = adj_center / cluster_size
	filtered_hotspots[n].filter_width /= cluster_size
	filtered_hotspots[n].dens_count = avg_dens_count / cluster_size
	filtered_hotspots[n].weighted_avg_sd = avg_sd / cluster_size

	return filtered_hotspots 

import stats

def refine_hotspots(filtered_hotspots, tags_count, mappable_count, window_size = 50000, window_size_small = 10000):

	uniq_positions = np.array( sorted(tags_count.keys()) )

	tags_count_total = np.sum( tags_count.values() )

	uniq_mappable_total = np.sum( mappable_count.values() )

	for n, current in filtered_hotspots.items():

		pos = current.avg_pos

		# count tags within hotspot window and set boundaries

		left = pos - (current.filter_width / 2)
		right = pos + (current.filter_width / 2)

		i = find_ge(uniq_positions, left)

		current.filter_left = uniq_positions[i]

		while i < len(uniq_positions) and uniq_positions[i] <= right:

			current.filter_tags += tags_count.get(uniq_positions[i], 0)

			i += 1

		current.filter_right = uniq_positions[i-1]

		# calculations for expected tags

		nwindows = window_size / window_size_small

		left = ( pos - (pos % window_size_small) ) -  ( (nwindows / 2) * window_size_small )
		right = left + window_size

		# get uniquely mappable sites

		uniq_mappable_in_window = 0

		for i in xrange(nwindows):

			window_total = mappable_count.get(left + (i * window_size_small), window_size_small)

			window_total = window_size_small if window_total > window_size_small else window_total

			uniq_mappable_in_window  += window_total
	
		uniq_mappable_in_window = window_size if uniq_mappable_in_window > window_size else uniq_mappable_in_window

		# get density in same window

		tags_in_window = 0

		i = find_ge(uniq_positions, left)

		while i < len(uniq_positions) and uniq_positions[i] <= right:

			tags_in_window += tags_count.get(uniq_positions[i], 0)

			i += 1

		# calc z-score

		(z_local, p_value_local) = stats.compute_z_score(current.filter_tags, current.filter_width, tags_in_window, uniq_mappable_in_window)

		(z_genome_wide, p_value_genome_wide) = stats.compute_z_score(current.filter_tags, current.filter_width, tags_count_total, uniq_mappable_total)
		
		if z_local < z_genome_wide:

			current.filter_z_score = z_local
			current.filter_p_value = p_value_local

		else:

			current.filter_z_score = z_genome_wide
			current.filter_p_value = p_value_genome_wide


import sys, os, argparse

parser = argparse.ArgumentParser(prog ="find_hotspots", description = "initial identification of regions within increased tag density")

parser.add_argument("--contig", action="store", required = True)
parser.add_argument("--contig_sizes", action="store", required = True)
parser.add_argument("--mappable_genome", action="store", required = True)

parser.add_argument("--tag_file", action="store", required = True, help = "tag file (5' base only) in bed3 format")

parser.add_argument("--tmpdir", action="store", default = "/tmp", help = "path to store temporary files (default: %(default)s)")

parser.add_argument("--window_min", action="store", type=int, default = 200, help = "minumum window size to look for hotspots (default: %(default)s)")
parser.add_argument("--window_max", action="store", type=int, default = 300, help = "maximum window size to look for hotspots (default: %(default)s)")
parser.add_argument("--window_step", action="store", type=int, default = 50, help = "window step-size to look for hotspots (default: %(default)s)")
parser.add_argument("--window_thresh", action="store", type=float, default = 3.0, help = "number of standard deviations to be considered as a potential hotspot (default: %(default)s)")
parser.add_argument("--window_thresh_fuzzy", action="store_true", help = "add some random noise to the z-score threshold to the boundary cases")

parser.add_argument("--density_window", action="store", type=int, default = 50000, help = "window size to compute background density (default: %(default)s)")
parser.add_argument("--density_window_small", action="store", type=int, default = 10000, help = "")

parser.add_argument("--ignore_duplicates", action="store_true", help = "ignore duplicate sequencing tags")

parser.add_argument("--random_seed", action="store", type=int, default = 1)


args = parser.parse_args()

contig = args.contig

# set random seed 

np.random.seed(args.random_seed)

#######################
# read chrom sizes
#######################

contig_sizes = {}

for line in open(args.contig_sizes):

	(cont, size) = line.strip().split('\t')

	contig_sizes[cont] = int(size)

#######################
# read mappable genome size
#######################

import subprocess

total_mappable = 0

proc = subprocess.Popen(["bedextract", contig, args.mappable_genome], stdout = subprocess.PIPE)

while True:

  line = proc.stdout.readline()
  
  if line == '': break
 
  (cont, start, end) = line.strip().split('\t')[:3]

  total_mappable += int(end) - int(start)

#######################
# read mappable positions density in windows
#######################

mappable_count = {}

tmpfile = os.path.join(args.tmpdir, "unique.mappable.%s.bed" % os.getpid())

f = open(tmpfile, "w")

for i in xrange(0, contig_sizes[contig], args.density_window_small): 

	print >> f, "%s\t%d\t%d" % (contig, i, (i + args.density_window_small) if i + args.density_window_small <= contig_sizes[contig] else contig_sizes[contig])

f.close()

proc0 = subprocess.Popen(["bedextract", contig, args.mappable_genome], stdout = subprocess.PIPE)
proc1 = subprocess.Popen(["bedmap", "--delim", "\t", "--echo", "--bases", tmpfile, "-"], stdin = proc0.stdout, stdout = subprocess.PIPE)

data = proc1.stdout

for line in data:
	
	(cont, start, end, count) = line.strip().split('\t')

	mappable_count[int(start)] = mappable_count.get(int(start), 0) + int(count)

#clean up

os.remove(tmpfile)

#######################
# read observed counts
#######################

tags_count = {}

proc = subprocess.Popen(["bedextract", contig, args.tag_file], stdout = subprocess.PIPE)

while True:

	line = proc.stdout.readline()

	if line == '': break

	(cont, start, end) = line.strip().split('\t')[:3]

	tags_count[int(start)] = (tags_count.get(int(start), 0) + 1) if not args.ignore_duplicates else 1

#######################
# run hotspot
#######################

print >> sys.stderr, "Finding putative hotspots..."

hotspots = find_hotspots(tags_count, contig_sizes[contig], args.window_thresh, args.window_min, args.window_max, args.window_step, args.window_thresh_fuzzy)

print >> sys.stderr, "Filtering hotspots..."

filtered_hotspots = filter_hotspots(hotspots)

print >> sys.stderr, "Calculating z-scores..."

refine_hotspots(filtered_hotspots, tags_count, mappable_count, args.density_window, args.density_window_small)

for n, current in filtered_hotspots.items():

	print "%s\t%d\t%d\t.\t%f\t%f\t%d\t%d\t%d" % (contig, current.filter_left, current.filter_right, current.filter_z_score, current.filter_p_value, current.avg_pos, current.filter_width, current.filter_tags)

