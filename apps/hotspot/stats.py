#/bin/env python

# Compute the z-score

import scipy.special

import numpy as np

def compute_z_score(tags, uniq_positions, tags_in_window, uniq_positions_in_window):

	try:

		# Probability of a mapable site

		p = float(uniq_positions) / float(uniq_positions_in_window)

		# Expected mean/sd amount of mappable tags

		mu = p * float(tags_in_window)
		sigma = np.sqrt(p * (1 - p) * float(tags_in_window))

		# Z

		z = (float(tags) - mu) / sigma
	
		# p

		pval = scipy.special.betainc(tags, tags_in_window - tags + 1, p)

	except:

		z = 0.0
		pval = 1.0

	return (z, pval)