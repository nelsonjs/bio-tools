#!/bin/env python

import sys, os, argparse

parser = argparse.ArgumentParser(prog ="generate_tag_libraries", description = "filters a raw bam file and produces an observed and random 5' tag library")

parser.add_argument("--contig", action="store", required = True)
parser.add_argument("--mappable_genome", action="store", required = True)
parser.add_argument("--alignment_file", action="store", required = True)
parser.add_argument("--observed_tag_file", action="store", required = True)
parser.add_argument("--random_tag_file", action="store", required = True)
parser.add_argument("--random_seed", action="store", type = int, default = 1)

args = parser.parse_args()

contig = args.contig

import numpy as np

# set seed

np.random.seed(args.random_seed)

#######################
# read observed counts
#######################

import pysam

import subprocess

tags_count_total = 0

tags = pysam.Samfile(args.alignment_file)

proc = subprocess.Popen(["sort-bed", "-"], stdin = subprocess.PIPE, stdout = open(args.observed_tag_file, "w"))

for tag in tags.fetch(contig):

	if tag.mapq == 0: continue

	try:

		i = (tag.aend - 1) if not tag.is_reverse else tag.pos

		proc.stdin.write("%s\t%d\t%d\n" % (contig, i, i + 1))

		tags_count_total += 1

	except:

		pass

proc.stdin.close()
proc.wait()

#######################
# read mappable genome size
#######################

mappable_positions = []

proc = subprocess.Popen(["bedextract", contig, args.mappable_genome], stdout = subprocess.PIPE)

while True:

  line = proc.stdout.readline()
  
  if line == '': break
 
  (cont, start, end) = line.strip().split('\t')[:3]

  for i in xrange(int(start), int(end)): mappable_positions.append(i)

#######################
# sample
#######################

proc = subprocess.Popen(["sort-bed", "-"], stdin = subprocess.PIPE, stdout = open(args.random_tag_file, "w"))

for i in xrange(tags_count_total + 1):

	i = np.random.randint( len(mappable_positions) )

	proc.stdin.write("%s\t%d\t%d\n" % (contig, mappable_positions[i], mappable_positions[i] + 1))

proc.stdin.close()
proc.wait()
