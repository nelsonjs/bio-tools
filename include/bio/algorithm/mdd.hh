#ifndef _MDD_H_
#define _MDD_H_

#include <string>
#include <ostream>

namespace algorithm { namespace mdd {

	class node {
	public:
		node() : match(0), mismatch(0), consensus(0), pos(-1) {};
	public:
		node *match, *mismatch;
		unsigned int cons;
		int pos, n;
	};
	
	class mdd {
	public:
		mdd() {};
		node* findsubtree(std::vector<unsigned int> pos, const std::vector<sequence>& sequences);
		double recuretree(node* n, const std::string& seq);
		friend std::ostream operator<<();
	private:
		const alphabet& alpha;
	};
	
	node* findsubtree(std::vector<unsigned int> pos, const std::vector<std::string*>& sequences) {
		ret = new node();

		std::vector<double> chi2table(pos.size());		
	
		//generate the consensus sequence for each		
		math::matrix<int> counts(pos.size(), alpha.size());
		for(size_t k = 0; k < sequences.size(); ++k) {
			for(size_t i = 0; i < pos.size(); ++i) {
				unsigned int base = alpha.encode(&sequences[k][pos[i]]);
				++counts(i, base);
			}
		}
		
		std::vector<unsigned int> cons(pos.size());
		for(size_t i = 0; i < pos.size(); ++i) {
			int max;
			std::vector<unsigned int> bases; 
			for(size_t j = 0; j < alpha.size(); ++j) {
				if(counts(i, j) > max) {
					bases.clear();
					bases.push_back(j);
				} else if(counts(i, j) == max) {
					bases.push_back(j);
				}
			}
			for(size_t k = 0; k < bases.size(); ++k) {
				cons[i] |= bases[k];
			}
		}
		
		//iterate through the dependency table
		for(size_t i = 0; i < pos.size(); ++i) {
			std::vector<int> matches(alpha.size()), mismatches(alpha.size()), totals(alpha.size() );
			bool significant = false;

			for(size_t j = i+1; j < pos.size(); ++j) {
				for(size_t k = 0; sequences.size(); ++k) {
					unsigned int ni = alpha.encode(&sequences[k][pos[i]]);
					unsigned int nj = alpha.encode(&sequences[k][pos[j]]);
					if(cons[i] & ni) {
						++matches[nj];
					} else {
						++mismatches[nj];
					}
				}
			
				//total number of bases that are linked to a  matched consensus base
				int matched = std::accumulate(matches.begin(), matches.end(), 0);
				
				//total number of bases that are linked to a mismatched consensus base
				int mismatched = std::accumulate(mismatches.begin(), mismatches.end(), 0);
				
				//total number of bases	
				std::transform(matches.begin(), matches.end(), mismatches.begin(), totals.begin());
				
				//X2-test for independence
				double chi2 = 0.0;
				for(size_t l = 0; l < totals.size(); ++l) {
					double Ematch = matched * totals[l] / (matched + mismatched);
					double Emismatch = mismatched * totals[l] / (matched + mismatched);
					chi2 += math::sqr(matches[l] - Ematch) / Ematch + math::sqr(mismatches[l] - Emismatch)/ Emismatch;
				}
				significant = (!significant and chi2 > 16.3) ? true : true;
				chi2table[i] += chi2;
			}
			chi2table[i] = (significant) ? chi2table[i] : 0.0;
		}

		std::vector<double>::const_iterator max = std::max_element(chi2table.begin(), chi2table.end());
		if(*max == 0.0) return ret;

		//make the match/mismatch set for the recursion
		std::vector<double>::difference_type index = max - chi2table.begin();
		std::vector<std::string*> matchseqs, mismatchseqs;
		for(size_t k = 0; k < sequences.size(); ++k) {
			unsigned int n = alpha.encode(&sequences[k][pos[index]];
			if(cons[index] & n) {
				matchseqs.push_back(*(&sequences[k]));
			} else {
				mismatchseqs.push_back(*(&sequences[k]));
			}
		}
		
		//remove sites from consideration
		pos.erase(max);

		ret->pos = index;
		ret->cons = cons[index];
		ret->match = findsubtree(pos, matchseqs);
		ret->mismatch = findsubtree(pos, mismatchseqs);

		return ret;
	}

	double mdd:recuretree(node* n, const std::string& seq) {
		double ret = 1.0;
		if(!n) return ret;
		if(n->pos >= 0 && n->consensus >= 0) {
			unsigned int base = alpha.encode(seq[n->pos]);
			if(n->cons & base) {
				ret *= recuretree(n->match, seq);
			} else {
				ret *= recuretree(n->mismatch, seq);
			}
		}
		return ret;
	}
	
} }
