#ifndef _MARKOV_HH_
#define _MARKOV_HH_

#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

#include "bio/alphabet/nucleotide.hh"

template<typename T = double>
class markov {
public:
	std::vector<T*> data;
	
public:
	markov(size_t order = 5) { resize(order); }
	~markov() {
		for (size_t i = 0; i < data.size(); ++i) {
			delete[] data[i];
		}
	}

	T* begin(size_t order) { return data[order]; }
	const T* begin(size_t order) const { return data[order]; }
	T* end(size_t order) { return data[order] + markov::requirement(order); }
	const T* end(size_t order) const { return data[order] + markov::requirement(order); }
	
	size_t order() const { return data.size() - 1; }
	
	void resize(size_t order) {
		size_t bases = order + 1;
		size_t psize = data.size();
		size_t nsize = bases;
		
		if (nsize < psize) {
			for (size_t i = nsize; i < psize; ++i) {
				if (data[i]) {
					delete[] data[i];
				}
			}
		}
		
		data.resize(nsize);
		
		for (size_t i = psize; i < nsize; ++i) {
			unsigned long r = markov::requirement(i);
			data[i] = new T[r];
			std::fill(data[i], data[i] + r, T(0));
		}
	}
	
	void normalize() {
		for (size_t i = 0; i < data.size(); ++i) {
			unsigned long r = markov::requirement(i);
			for (unsigned long j = 0; j < r; j = j + 4) {
				T sum = T(0);
				for (unsigned int k = 0; k < 4; ++k) { sum += data[i][j + k]; }
				for (unsigned int k = 0; k < 4; ++k) { data[i][j + k] /= sum; }
			}
		}
	}
	
	void pseudocount(const T& val) {
		for (size_t i = 0; i < data.size(); ++i) {
			unsigned long r = markov::requirement(i);
			for (unsigned long j = 0; j < r; ++j) {
				data[i][j] += val;
			}
		}
	}
	
	void update(const std::string& sequence) {
		unsigned long lookahead = 0;
		unsigned long index = 0;
		for (std::string::size_type i = 0; i < sequence.size(); ++i) {
			const unsigned int id = DNA.encode(sequence[i]);
			if (id < 4) {
				index = (index << 2);
				index |= id;
				for (unsigned long l = 0; l <= lookahead; ++l) {
					data[l][index & markov::mask(l + 1)]++;
				}
				if (lookahead < order()) {
					++lookahead;
				}
			} else {
				lookahead = 0;
				index = 0;
			}
			
		}
	}
		
	T likelihood(const std::string& sequence) const {
		const unsigned long mask = markov::mask(data.size());
		unsigned int lookahead = 0;
		unsigned long index = 0;
		T res = 1;
		
		for (std::string::size_type i = 0; i < sequence.size(); ++i) {
			const unsigned int id = DNA.encode(sequence[i]);
			if(id < 4) {
				index = (index << 2);
				index |= id;
				res *= data[lookahead][index];
				
				if (res == T(0)) { return res; }
				if (lookahead < order()) { ++lookahead;	}
			} else {
				lookahead = 0;
				index = 0;
			}
		}
		return res;
	}
	
	template<typename S>
	friend std::ostream& operator<<(std::ostream& stream, const markov<S>& model);
	
private:
	static unsigned long mask(unsigned long i) {
		return ~(-1 << (2 * i));
	}
	
	static unsigned long requirement(size_t order) {
		return 1 << (2 * (order + 1));
	}
	
	static  unsigned long index(const std::string& sequence) {
		const unsigned long mask = markov::mask(sequence.size());
		unsigned long index = 0;
		for (std::string::size_type i = 0; i < sequence.size(); ++i) {
			index = (index << 2);
			index |= DNA.encode(sequence[i]);
			index &= mask;
		}
		return index;
	}	
	
	static std::string sequence(unsigned long index, size_t n) {
		std::string res;
		for (size_t i = 0; i < n; ++i) {
			res = DNA.decode(index & 3) + res;
			index = (index >> 2);
		}
		return res;
	}
};


template<typename S>
inline std::ostream& operator<<(std::ostream& stream, const markov<S>& model) {
	for (size_t i = 0; i < model.data.size(); ++i) {
		stream << "#[order:" << i << "]" << std::endl;
		unsigned long r = markov<S>::requirement(i);
		for (unsigned long j = 0; j < r; ++j) {
			stream << markov<S>::sequence(j, i + 1) << " " << std::setprecision(3) << model.data[i][j] << std::endl;
		}
	}
	return stream;
}

#endif
