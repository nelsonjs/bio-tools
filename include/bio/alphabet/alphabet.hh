#ifndef _ALPHABET_HH_
#define _ALPHABET_HH_

#include <string>
#include <cctype>
#include <functional>

struct mapper : public std::unary_function<const char, char> {
  char table[256];
  mapper(const std::string from, const std::string to, bool cs = true) {
    for(unsigned int i = 0; i < 256; ++i) {
      table[i] = i;
    }
    for(unsigned int i = 0; i < from.size(); ++i) {
	if(cs) {
		table[static_cast<unsigned char>(from[i])] = to[i];
      	} else {
		table[static_cast<unsigned char>(std::toupper(from[i]))] = std::toupper(to[i]);
		table[static_cast<unsigned char>(std::tolower(from[i]))] = std::tolower(to[i]);
      }
    }
  }
  char operator()(const char c) const {
    return table[static_cast<unsigned char>(c)];
  }
};

struct encoder : public std::unary_function<const char, char> {
	unsigned char table[256];
	encoder(const std::string chars, bool cs = true) {
		init(chars, cs);
	}

	encoder(const std::string chars, bool cs, char nonmember) {
		init(chars, cs);
		unsigned char nmupper = table[static_cast<unsigned int>(std::toupper(nonmember))];
		unsigned char nmlower = table[static_cast<unsigned int>(std::tolower(nonmember))]; 
		for(unsigned int i = 0; i < 256; ++i) {
			if(table[i] == 0xFF) {
				if(std::isupper(static_cast<char>(i))) {
					table[i] = nmupper;
				} else {
					table[i] = nmlower;
				}
			}
		}
	}
	void init(const std::string& chars, bool cs) {
		for(unsigned int i = 0; i < 256; ++i) {
  			table[i] = 0xFF;
		}
		for(unsigned int i = 0; i < chars.size(); ++i) {
			if(cs) {
				table[static_cast<unsigned char>(chars[i])] = i;
			} else {
				table[static_cast<unsigned char>(std::toupper(chars[i]))] = std::toupper(i);
				table[static_cast<unsigned char>(std::tolower(chars[i]))] = std::tolower(i);
      			}
    		}
  	}
	
  char operator()(const char c) const {
    return table[static_cast<unsigned char>(c)];
  }
  bool is_member(const char c) const {
    return (table[static_cast<unsigned char>(c)] != 0xFF);
  }
};

struct decoder : public std::unary_function<const unsigned char, char> {
  const std::string table;
  decoder(const std::string s) : table(s) {}
  char operator()(const unsigned char c) const {
    return table[c];
  }
};

class alphabet {
public:
  virtual ~alphabet() {};
  virtual unsigned int size() const = 0;
  virtual bool is_member(const char c) const = 0;
  virtual bool is_equal(const char a, const char b) const = 0;
  
  virtual unsigned char encode(const char c) const = 0;
  virtual std::string encode(const std::string& s) const;
  virtual char decode(const unsigned char c) const = 0;
  virtual std::string decode(const std::string& s) const;
	virtual std::string hard_mask(std::string s) const = 0;
	virtual std::string unmask(std::string s) const = 0;
};

inline std::string alphabet::encode(const std::string& s) const {
  std::string e(s);
  for(std::string::iterator i = e.begin(); i != e.end(); ++i) {
    *i = encode(*i);
  }
  return e;
}

inline std::string alphabet::decode(const std::string& s) const {
  std::string d(s);
  for(std::string::iterator i = d.begin(); i != d.end(); ++i) {
    *i = decode(*i);
  }
  return d;
}

#endif

