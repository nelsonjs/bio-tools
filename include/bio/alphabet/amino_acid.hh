#ifndef _AMINO_ACID_HH_
#define _AMINO_ACID_HH_

#include "alphabet.hh"

class amino_acid : public alphabet {
public:	
	amino_acid(const std::string& chars)
	: dec(chars), enc(chars, false) {}

	unsigned int size() const { return dec.table.size(); }	
	bool is_member(const char c) { enc.is_member(c); }
	
	unsigned char encode(const char base) const { return enc(c); }
	char decode(const unsigned char c) const { return dec(c); }

private:
	decoder dec;
	encoder enc;
};

const amino_acid PROTEIN("ARNDCQEGHILKMFPSTWYVBZX");

#endif

