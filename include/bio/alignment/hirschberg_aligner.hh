#ifndef _HIRSCHBERG_ALIGNER_HH_
#define _HIRSCHBERG_ALIGNER_HH_

#include <algorithm>

#include "pairwise_sequence_aligner.hh"
#include "score_matrix.hh"

template<typename T = double>
class hirschberg_aligner : public pairwise_sequence_aligner<T> {
public:
	hirschberg_aligner(const score_matrix<T>& sim, T gap)
	: sim(sim), gap(gap) {}
 
	pairwise_alignment align(const std::string& a, const std::string& b) {
		if(a.size() == 0) {
			return pairwise_alignment(std::string(b.size(), '-'), b);
		} else if (b.size() == 0) {
			return pairwise_alignment(a, std::string(a.size(), '-'));
		} else if (a.size() == 1 and b.size() == 1) {
			T aligned = sim.get_char_score(a[0], b[0]);
			T gapped = gap + gap;
			return (aligned >= gapped) ? pairwise_alignment(a, b) : 
			  pairwise_alignment(a + "-", "-" + b);
		} else if (a.size() == 1) {
			pairwise_alignment alignment = align(b, a);
			return alignment.swap();		
		} else {
			std::vector<T> forward, backward;

			std::string::size_type middle = a.size() / 2;
			std::string atop = a.substr(0, middle);
	
			std::string abottom = a.substr(middle);
			std::reverse(abottom.begin(), abottom.end());			
			std::string brev = b;
			std::reverse(brev.begin(), brev.end());	

#pragma omp task shared(atop, b, forward)		
			score_last_row(atop, b, forward);
#pragma omp task shared(abottom, brev, backward)
			score_last_row(abottom, brev, backward);
#pragma omp taskwait

			std::reverse(abottom.begin(), abottom.end());
			std::reverse(backward.begin(), backward.end());

			std::transform(forward.begin(), forward.end(), backward.begin(), forward.begin(), std::plus<T>());
			size_t max = std::max_element(forward.begin(), forward.end()) - forward.begin();

			std::string bleft = b.substr(0, max);
			std::string bright = b.substr(max);

			pairwise_alignment left, right;

#pragma omp task shared(left)
			left = align(atop, bleft);
#pragma omp task shared(right)
			right = align(abottom, bright);
#pragma omp taskwait

			return left + right;
		}
	}

	T score(const std::string& a, const std::string& b) {
		std::vector<T> row;
		score_last_row(a, b, row);
		return row[row.size()-1];	
	}

private:
	void score_last_row(const std::string& a, const std::string& b, std::vector<T>& row) {
		row.resize(b.size()+1);
		row[0] = T();
		
		for(size_t i = 1; i < row.size(); ++i) {
			row[i] = gap + row[i-1];
		}

		T up, diag;
		for(size_t i = 1; i <= a.size(); ++i) {
			diag = row[0];
			row[0] += gap;
			for(size_t j = 1; j <= b.size(); ++j) {
				up = row[j];
				row[j] = std::max(diag + sim.get_char_score(a[i-1], b[j-1]), std::max(row[j-1], up) + gap);
				diag = up;
			}
		}
	}

private:
	const score_matrix<T>& sim;
	T gap;
};

#endif
