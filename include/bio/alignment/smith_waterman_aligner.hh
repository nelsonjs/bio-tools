#ifndef _SMITH_WATERMAN_ALIGNER_HH_
#define _SMITH_WATERMAN_ALIGNER_HH_

#include "pairwise_sequence_aligner.hh"
#include "misc/dpmatrix.hh"
#include "math/functions.hh"

using namespace misc;
using namespace math;

template<typename T = double>
class smith_waterman_aligner : public pairwise_sequence_aligner<T> {
protected:
	T match, mismatch, gap;
	dpmatrix<T> H;

	const alphabet& alpha;
public:
	smith_waterman_aligner(T match, T mismatch, T gap, const alphabet& alpha)
	: match(match), mismatch(mismatch), gap(gap), alpha(alpha) {}

	virtual pairwise_alignment align(const std::string& a, const std::string& b) {
		
		if ((a.size() != H.rows() - 1) or (b.size() != H.cols() - 1)) {
			H = dpmatrix<T>(a.length() + 1, b.length() + 1);
		}
		
		for (size_t i = 1; i < a.length()+1; ++i) {
			for (size_t j = 1; j < b.length()+1; ++j) {
				T left = H(i, j-1).value + gap;
				T diag = H(i-1, j-1).value + (this->alpha.is_equal(a[i-1],b[j-1]) ? match : mismatch);
				T up = H(i-1, j).value + gap;				

				T m = H(i, j).value = max<T, 4>(T(0), left, diag, up);
				
				if (m == diag) H(i, j).trace = &H(i-1, j-1);
				else if (m == left) H(i, j).trace = &H(i, j-1);
				else if (m == up) H(i, j).trace = &H(i-1, j);
			}
		}

		pairwise_alignment alignment;

		dpcell<T>* cell = std::max_element(H.begin(), H.end());

		alignment.score = double(cell->value);
		alignment.enda = cell->i-1;
		alignment.endb = cell->j-1;
				
		while(cell->trace) {
			if(cell->i == cell->trace->i+1 && cell->j == cell->trace->j+1)	{
				alignment.a += a[cell->i-1];
				alignment.b += b[cell->j-1];
				if (this->alpha.is_equal(a[cell->i-1], b[cell->j-1])) { 
					alignment.matches += 1; 
				} else {
					alignment.mismatches += 1;
				}
			} else if (cell->i == cell->trace->i) {
				alignment.a += '-';
				alignment.b += b[cell->j-1];
				alignment.gaps++;
			} else if (cell->j == cell->trace->j) {
				alignment.a += a[cell->i-1];
				alignment.b += '-';
				alignment.gaps++;
			}
			cell = cell->trace;
		}

		alignment.starta = cell->i;
		alignment.startb = cell->j;
		
		alignment.reverse();
		return alignment;
	}
};

#endif
