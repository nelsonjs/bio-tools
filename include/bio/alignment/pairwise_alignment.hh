#ifndef _PAIRWISE_ALIGNMENT_HH_
#define _PAIRWISE_ALIGNMENT_HH_

#include <string>
#include <iostream>

#define LINE_WIDTH 100

class pairwise_alignment {
public:
	pairwise_alignment()
	: gaps(0), mismatches(0), matches(0) {}
	
	pairwise_alignment(const std::string& a, const std::string& b) : a(a), b(b) {}
	
	pairwise_alignment& operator+=(const pairwise_alignment& other) {
		a += other.a;
		b += other.b;
		return *this;
	}

	pairwise_alignment& swap() {
		std::swap(a, b);
		return *this;
	}

	pairwise_alignment operator+(const pairwise_alignment& other) const {
		pairwise_alignment ret = *this;
		return ret += other; 
	}

	void reverse() {
		std::reverse(a.begin(), a.end());
		std::reverse(b.begin(), b.end());
	}

	size_t size() const {
		return (a.size() > b.size()) ? a.size() : b.size();
	}

public:
	std::string a, b;
	int starta, enda, startb, endb;
	int gaps, mismatches, matches;
	double score;
};

inline std::ostream& operator<<(std::ostream& stream, const pairwise_alignment& alignment) {
	//determine number of rows needed
	size_t nrows = alignment.size() / LINE_WIDTH;
	if((alignment.size() % LINE_WIDTH)) ++nrows;

	std::string annotation(alignment.size(), ' ');
	for(size_t i = 0; i < alignment.size(); ++i) {
		if(alignment.a[i] != alignment.b[i]) annotation[i] = '*';
	}
	
	if(nrows == 1) {
		stream << alignment.starta << "\t" << alignment.a << "\t" << alignment.enda << "\n" 
			<< alignment.startb << "\t" << alignment.b << "\t" << alignment.endb << "\n"
			<< "ANNOT\t" << annotation << "\n\n";
	} else {
		for(size_t i = 0; i < nrows; ++i) {
			if(i == 0) {
				stream << alignment.starta << "\t" << alignment.a.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n" 
					<< alignment.startb << "\t" << alignment.b.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n"
					<< "ANNOT\t" << annotation.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n\n";
			} else if(i == nrows - 1) {
				stream << alignment.a.substr(i*LINE_WIDTH, LINE_WIDTH) << "\t" << alignment.enda << "\n" 
					<< alignment.b.substr(i*LINE_WIDTH, LINE_WIDTH) << "\t" << alignment.endb << "\n"
					<< annotation.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n\n";
			} else {
				stream << alignment.starta + i * LINE_WIDTH  << "\t" << alignment.a.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n" 
					<< alignment.startb + i * LINE_WIDTH << "\t" << alignment.b.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n"
					<< "ANNOT\t" << annotation.substr(i*LINE_WIDTH, LINE_WIDTH) << "\n\n";
			}
		}
	}
	stream << "SCORE\t" << alignment.score << "MATCH\t" << alignment.matches << "\n";
	
	return stream;
}

#endif
