#ifndef _BED_RECORD_HH_
#define _BED_RECORD_HH_

namespace io { namespace bed {
	
	struct record {
		record(const std::string& chrom = "", long start = 0, long end = 0, const std::string& id = "", char strand = '+', double signal = 0.0) 
		: chrom(chrom), start(start), end(end), id(id), strand(strand), signal(signal) {}
		
		std::string chrom, id;
		char strand;
		long start, end;
		double signal;
	};
} }

#endif

