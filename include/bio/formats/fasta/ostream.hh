#ifndef _FASTA_OSTREAM_HH_
#define _FASTA_OSTREAM_HH_

#include "record.hh"

namespace io { namespace fasta {
	
	class ostream {
	public:
		ostream(std::ostream& stream, size_t width = DEFAULT_LINE_WIDTH) : stream(stream), width(width) {}
	
		ostream& operator<<(const record& rec) {
			stream << TITLE_LINE_PREFIX << rec.id << std::endl;
			for(size_t start = 0; start < rec.sequence.size(); start += width) {
				stream << rec.sequence.substr(start, width) << std::endl;	
			}				
			return *this;
		}

	protected:
		size_t width;
		std::ostream& stream;
	};

} }

#endif
