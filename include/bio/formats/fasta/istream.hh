#ifndef _FASTA_ISTREAM_HH_
#define _FASTA_ISTREAM_HH_

#include <vector>
#include <string>
#include <iosfwd>
#include <istream>
#include <algorithm>

#include "misc/string.hh"
#include "record.hh"

using namespace misc::string;

namespace io { namespace fasta {
	
	class istream {
	public:
	  istream(std::istream& stream, const size_t buffer_len = (4 * 1024));

	  istream& operator>>(record& rec);  

	  operator bool() const;
	  bool operator!() const;

	private:
	  void fill_buffer();
	  
	private:
	  std::istream& stream;
	  std::vector<char> buffer;
	  std::vector<char>::iterator pos;
	  bool end;
	  bool whitespace;
	};
	
	inline istream::istream(std::istream& stream, const size_t buffer_len) 
  	: stream(stream), buffer(buffer_len), pos(buffer.end()), whitespace(false) {
		end = false;
 	
  	while(stream && pos == buffer.end()) {
  	  fill_buffer();
  	  pos = std::find(buffer.begin(), buffer.end(), TITLE_LINE_PREFIX);
  	}
	
  	if(pos == buffer.end()) {
  	  end = true;
  	}
	}
	
	inline istream::operator bool() const {
	  return !end;
	}
	
	inline bool istream::operator!() const {
		  return end;
	}
	
	inline void istream::fill_buffer() {
	  stream.read(&buffer[0], buffer.size());
	  if(static_cast<size_t>(stream.gcount()) < buffer.size()) {
	    buffer.resize(stream.gcount());
	  }
	}
	
	inline istream& istream::operator>>(record& rec) {
	  if(pos == buffer.end()) {
	    end = true;
	    return *this;
	  }
	
		rec.id = "";
		rec.sequence = "";
	
	  std::vector<char>::iterator start = pos + 1;
	  pos = std::find(start, buffer.end(), '\n');
	
	  while(stream && pos == buffer.end()) {
	    rec.id.append(start, pos);
	    fill_buffer();
	    start = buffer.begin();
	    pos = std::find(start, buffer.end(), '\n');
	  }
	  rec.id.append(start, pos);
		
		strip_right(rec.id);	
		
	  start = pos + 1;
	  pos = std::find(start, buffer.end(), TITLE_LINE_PREFIX);
	  
	  while(stream && pos == buffer.end()) {
	    rec.sequence.append(start, std::remove_if(start, pos, is_whitespace()));
	    fill_buffer();
	    start = buffer.begin();
	    pos = std::find(start, buffer.end(), TITLE_LINE_PREFIX);
	  }
	  rec.sequence.append(start, std::remove_if(start, pos, is_whitespace()));
	
	  return *this;
	}
	
} }
#endif		
