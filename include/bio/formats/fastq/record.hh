#ifndef _FASTQ_RECORD_HH_
#define _FASTQ_RECORD_HH_

namespace io { namespace fastq {
	struct record {
  	record(const std::string& ident = "", const std::string& sequence = "")
  	  : identifier(ident), sequence(sequence) {};

  	std::string identifier;
  	std::string sequence;
  	std::string quality;
	};
} }

#endif
