#ifndef _FASTQ_OSTREAM_
#define _FASTQ_OSTREAM_

#include <vector>
#include <string>
#include <iosfwd>
#include <ostream>
#include <algorithm>

#include "constants.hh"
#include "record.hh"

namespace io { namespace fastq {

	class ostream {
	public:
	
  	ostream(std::ostream& stream);
  	ostream& operator<<(record& rec);  
	
 	protected:
  	std::ostream& stream;
	};


	inline ostream::ostream(std::ostream& stream)
  	: stream(stream) {
 	}

	inline ostream& ostream::operator<<(record& rec) {
 		stream << IDENTIFIER_LINE_PREFIX << rec.identifier << std::endl;
		stream << rec.sequence << std::endl;
		//stream << QUALITY_LINE_PREFIX << rec.identifier << std::endl;
		stream << QUALITY_LINE_PREFIX << std::endl;
		stream << rec.quality << std::endl;
		return *this;
	}

} }

#endif
