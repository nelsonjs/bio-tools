#ifndef _MAX_PLUS_HH_
#define _MAX_PLUS_HH_

template<typename T>
class max_plus {
	max_plus& operator*=(const max_plus& other);
	max_plus& operator+=(const max_plus& other);

	max_plus operator*(const max_plus& other) const;
	max_plus operator+(const max_plus& other) const;

	bool operator<(const max_plus& other) const;
	bool operator<=(const max_plus& other) const;
	bool operator==(const max_plus& other) const;

	T value;
}

template<typename T>
max_plus& max_plus::operator*=(const max_plus& other) {
	return *this;
}

template<typename T)
max_plus& max_plus::operator+=(const max_plus& other) {
	return *this;
}
