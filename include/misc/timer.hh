#ifndef _TIMER_HH_
#define _TIMER_HH_

#include <ctime>

namespace utils {
	
class timer {
public:
	timer() {
		start = std::clock();
	}
	
	void reset() {
		start = std::clock();
	}

	double get_time() {
		return (std::clock() - start) / (double)CLOCKS_PER_SEC;
	}
protected:
	clock_t start;
};
	
}

#endif
