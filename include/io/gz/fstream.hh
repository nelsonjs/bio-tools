#ifndef _GZ_FSTREAM_HH_
#define _GZ_FSTREAM_HH_

#include <iostream>
#include <fstream>
#include <cstring>

#include <zlib.h>

namespace io { namespace gz {
	
	//filebuf
	class filebuf : public std::streambuf {
	private:
		static const int size = 47 + 256;
		gzFile file;
		char buffer[size];
		int opened;
		int mode;

		int flush_buffer();
	public:
		filebuf() 
			: opened(0) {
			setp(buffer, buffer + (size - 1));
			setg(buffer + 4, buffer + 4, buffer + 4);		
		}
		
		int is_open() { return opened; }
		filebuf* open(const std::string& path, int open_mode);
		filebuf* close();
		~filebuf() { close(); }

		virtual int overflow(int c = EOF);
		virtual int underflow();
		virtual int sync();
	};

	//fstream base
	class fstream : virtual public std::ios {
	protected:
		filebuf buf;
	public:
		fstream() { std::ios::init(&buf); }
		~fstream() {
			buf.close();
		}

		void open(const std::string& path, int open_mode) {
			if(not buf.open(path, open_mode)) {
				std::ios::clear(rdstate() | std::ios::badbit);
			}
		}

		void close() {
			if(buf.is_open()) {
				if(not buf.close()) {
					std::ios::clear(rdstate() | std::ios::badbit);
				}
			}
		}
		
		filebuf* rdbuf() { return &buf; }
	};

	//implementation
	inline filebuf* filebuf::open(const std::string& path, int open_mode) {
		if(is_open()) {
			return (filebuf*)0;
		}

		mode = open_mode;
		if((mode & std::ios::ate) or (mode & std::ios::app) or ((mode & std::ios::in) and (mode & std::ios::out))) {
			return (filebuf*)0;
		}

		char fmode[10];
		char* fmodeptr = fmode;
		if(mode & std::ios::in) {
			*fmodeptr++ = 'r';
		} else if(mode & std::ios::out) {
			*fmodeptr++ = 'w';
		}
		*fmodeptr++ = 'b';
		*fmodeptr = '\0';

		file = gzopen(path.c_str(), fmode);
		if(file == 0) {
			return (filebuf*)0;
		}

		opened = 1;
		return this;
	}
	
	inline filebuf* filebuf::close() { 
		if(is_open()) {
			sync();
			opened = 0;
			if(gzclose(file) == Z_OK) {
				return this;
			}
		}
		return (filebuf*)0;
	}

	inline int filebuf::underflow() {
		if(gptr() && (gptr() < egptr())) {
			return *reinterpret_cast<unsigned char*>(gptr());
		}

		if(not (mode & std::ios::in) or not opened) {
			return EOF;
		}
	
		int nputback = gptr() - eback();
		if(nputback > 4) {
			nputback = 4;
		}		
		std::memcpy(buffer + (4 - nputback), gptr() - nputback, nputback);

		int num = gzread(file, buffer + 4, size - 4);
		if(num <= 0) {
			return EOF;
		}

		setg(buffer + (4 - nputback), buffer + 4, buffer + 4 + num);
		return *reinterpret_cast<unsigned char*>(gptr());
	}

	inline int filebuf::flush_buffer() {
		int w = pptr() - pbase();
		if(gzwrite(file, pbase(), w) != w) {
			return EOF;
		}
		pbump(-w);
		return w;
	}

	inline int filebuf::overflow(int c) {
		if(not (mode & std::ios::out) or not opened) {
			return EOF;
		}

		if(c != EOF) {
			*pptr() = c;
			pbump(1);
		}
		
		if(flush_buffer() == EOF) {
			return EOF;
		}

		return c;
	}

	inline int filebuf::sync() {
		if(pptr() and pptr() > pbase()) {
			if(flush_buffer() == EOF) {
				return -1;
			}
		}
		return 0;
	}

} }
#endif
