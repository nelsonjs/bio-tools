#ifndef _IFSTREAM_HH_
#define _IFSTREAM_HH_

#include <fstream>
#include <stdexcept>

namespace io {
	
	template<class charT, class traits = std::char_traits<charT> >
	class basic_ifstream : public std::basic_ifstream<charT, traits> {
	public:
		basic_ifstream() {}
		explicit basic_ifstream(const std::string& path, std::ios_base::openmode mode = std::ios_base::in)
		: std::basic_ifstream<charT, traits>(path.c_str(), mode) {
			if(not *this) {
				throw std::runtime_error("Could not open input file: " + path);
			}
		}
		
		void open(const std::string& path, std::ios_base::openmode mode = std::ios_base::in) {
			std::basic_ifstream<charT, traits>::open(path.c_str(), mode);
			if(not *this) {
				throw std::runtime_error("Could not open input file: " + path);
			}
		}
		
		virtual ~basic_ifstream() {}
	};
	
	typedef basic_ifstream<char> ifstream;
	
}

#endif
